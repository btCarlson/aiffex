package aiffex

import (
	"encoding/binary"
	"fmt"
	"io"
	"os"
)

type AIFF struct {
	Name string
	Path string
	File *os.File
	Form *Form
}

type Form struct {
	Header *FormHeader
	Chunks []*Chunk
}

type FormHeader struct {
	ID   string
	Size uint32
	Type string
}

type Header struct {
	ID     string
	Size   uint32
	Offset uint32
}

type Chunk struct {
	Header *Header
	Data   []byte
}

type ID3Header struct {
	FileIdentifier string
	Version        string
	Flags          ID3HeaderFlags
	Size           uint32
}

type ID3HeaderFlags struct {
	Unsync       bool
	ExtHeader    bool
	Experimental bool
}

func ReadAIFF(filepath string) (*AIFF, error) {
	file, err := os.Open(filepath)
	if err != nil {
		return nil, err
	}

	rs := io.ReadSeeker(file)

	form, err := decodeForm(rs)
	if err != nil {
		return nil, err
	}

	aiff := AIFF{
		Name: filepath,
		File: file,
		Form: form,
	}

	return &aiff, nil
}

func decodeForm(rs io.ReadSeeker) (*Form, error) {
	// The FORM chunk is the container for the entire AIFF file, so it's header should be read starting at the first byte.
	formHeader, err := decodeFormHeader(rs)
	if err != nil {
		return nil, fmt.Errorf("error occured while decoding the FORM header: %w", err)
	}

	chunks, err := decodeChunks(rs)
	if err != nil {
		return nil, fmt.Errorf("error occured while decoding data chunks: %w", err)
	}

	form := Form{
		Header: formHeader,
		Chunks: chunks,
	}

	return &form, nil
}

func decodeFormHeader(rs io.ReadSeeker) (*FormHeader, error) {
	headerBytes := make([]byte, FORM_HEADER_SIZE)
	_, err := io.ReadFull(rs, headerBytes[:])
	if err != nil {
		return nil, err
	}

	header := FormHeader{
		ID:   string(headerBytes[0:HEADER_ID_SIZE]),
		Size: binary.BigEndian.Uint32(headerBytes[HEADER_ID_SIZE:HEADER_SIZE]),
		Type: string(headerBytes[HEADER_SIZE : HEADER_SIZE+HEADER_ID_SIZE]),
	}

	return &header, nil
}

func decodeChunks(rs io.ReadSeeker) ([]*Chunk, error) {
	offset := FORM_HEADER_SIZE
	var chunks []*Chunk
	for {
		chunkHeader, err := decodeHeader(rs, offset)
		if err != nil {
			if err == io.EOF || err == io.ErrUnexpectedEOF {
				break
			}
			return nil, err
		}

		chunkDataBytes := make([]byte, chunkHeader.Size)
		_, err = io.ReadFull(rs, chunkDataBytes[:])
		if err != nil {
			if err == io.EOF || err == io.ErrUnexpectedEOF {
				break
			}
			return nil, err
		}

		chunks = append(chunks, &Chunk{
			Header: chunkHeader,
			Data:   chunkDataBytes,
		})

		offset += chunkHeader.Size + HEADER_SIZE
		if chunkHeader.Size%2 != 0 {
			offset += 1
			rs.Seek(1, io.SeekCurrent)
		}
	}

	return chunks, nil
}

func decodeHeader(rs io.ReadSeeker, offset uint32) (*Header, error) {
	headerBytes := make([]byte, HEADER_SIZE)
	_, err := io.ReadFull(rs, headerBytes[:])
	if err != nil {
		return nil, err
	}

	header := Header{
		ID:     string(headerBytes[0:HEADER_ID_SIZE]),
		Size:   binary.BigEndian.Uint32(headerBytes[HEADER_ID_SIZE : HEADER_ID_SIZE+HEADER_SIZE_SIZE]),
		Offset: offset,
	}

	return &header, nil
}

// Close should be called at the end of the AIFF object's life.
func (a *AIFF) Close() {
	a.File.Close()
}

func (a *AIFF) DecodeID3Chunk() (ID3Header, map[string]string, error) {
	// Find the ID3 header
	var chunk *Chunk
	for _, c := range a.Form.Chunks {
		if c.Header.ID == ID3_CHUNK_ID {
			chunk = c
		}
	}

	bytes := chunk.Data

	// Decode ID3 header
	headerBytes := bytes[:ID3_HEADER_SIZE]
	// The first three bytes should always be "ID3" for ID3v2
	fileIdentifier := string(headerBytes[:3])
	// The next two bytes represent the major and minor version of the ID3 specification.
	version := fmt.Sprintf("ID3v2.%v.%v", headerBytes[3], headerBytes[4])
	// The next byte holds flags.
	unsync := headerBytes[5]&128 > 0
	extHeader := headerBytes[5]&64 > 0
	experimental := headerBytes[5]&32 > 0
	// The last four bytes contain the chunk size. The 7th bit of each byte is ignored.
	size := uint32(headerBytes[9]) + 128*uint32(headerBytes[8]) + 128*128*uint32(headerBytes[7]) + 128*128*128*uint32(headerBytes[6])

	header := ID3Header{
		FileIdentifier: fileIdentifier,
		Version:        version,
		Flags: ID3HeaderFlags{
			Unsync:       unsync,
			ExtHeader:    extHeader,
			Experimental: experimental,
		},
		Size: size,
	}

	// Decode ID3 frames
	id3FrameBytes := bytes[ID3_HEADER_SIZE:]
	tags := make(map[string]string, 0)
	offset := uint32(0)
	for offset < uint32(len(id3FrameBytes)-int(ID3_HEADER_SIZE)) {
		frameHeaderBytes := id3FrameBytes[offset : offset+ID3_HEADER_SIZE]
		frameID := string(frameHeaderBytes[:4])
		frameSize := uint32(frameHeaderBytes[7]) + 128*uint32(frameHeaderBytes[6]) + 128*128*uint32(frameHeaderBytes[5]) + 128*128*128*uint32(frameHeaderBytes[4])

		offset += ID3_HEADER_SIZE
		frameBytes := id3FrameBytes[offset : offset+frameSize]

		offset += frameSize

		tags[ID3CommonTags[frameID]] = string(frameBytes)
	}

	return header, tags, nil
}
