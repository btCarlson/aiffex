# aiffex

This package is an Audio Interchange File Format (AIFF) decoder. It can read in AIFF files, decode the chunks, and output metadata in plain human-readable English. This can be useful for media applications intended to handle this type of file, for example to organize a music library.

https://en.wikipedia.org/wiki/Audio_Interchange_File_Format