package aiffex

const (
	// AIFF is the expected form type encoded within the FORM header.
	AIFF_FORM_TYPE = "AIFF"
	// HEADER_SIZE is the standard size of a data chunk header.
	HEADER_SIZE = uint32(8)
	// FORM_HEADER_SIZE is the size of AIFF FORM container header, which includes 4 extra bytes to represent the FORM type.
	// The FORM type should always be 'AIFF' for AIFF files.
	FORM_HEADER_SIZE = uint32(12)
	//
	HEADER_ID_SIZE = uint32(4)
	//
	HEADER_SIZE_SIZE = uint32(4)
	//
	HEADER_FORM_TYPE_SIZE = uint32(4)
	//
	ID3_CHUNK_ID = "ID3 "
	//
	ID3_HEADER_SIZE = uint32(10)
)

var ID3CommonTags = map[string]string{
	"AENC": "Audio Encryption",
	"APIC": "Attached Picture",
	"COMM": "Comments",
	"COMR": "Commercial frame",
	"ENCR": "Encryption method registration",
	"EQUA": "Equalization",
	"ETCO": "Event timing codes",
	"GEOB": "General encapsulated object",
	"GRID": "Group identification registration",
	"IPLS": "Involved people list",
	"LINK": "Linked information",
	"MCDI": "Music CD identifier",
	"MLLT": "MPEG location lookup table",
	"OWNE": "Ownership frame",
	"PRIV": "Private frame",
	"PCNT": "Play counter",
	"POPM": "Popularimeter",
	"POSS": "Position synchronisation frame",
	"RBUF": "Recommended buffer size",
	"RVAD": "Relative volume adjustment",
	"RVRB": "Reverb",
	"SYLT": "Synchronized lyric/text",
	"SYTC": "Synchronized tempo codes",
	"TALB": "Album/Movie/Show title",
	"TBPM": "BPM",
	"TCOM": "Composer",
	"TCON": "Content type",
	"TCOP": "Copyright",
	"TDAT": "Date",
	"TDLY": "Playlist delay",
	"TENC": "Encoded by",
	"TEXT": "Lyricist",
	"TFLT": "File type",
	"TIME": "Time",
	"TIT1": "Content group description",
	"TIT2": "Title",
	"TIT3": "Subtitle",
	"TKEY": "Initial key",
	"TLAN": "Language",
	"TLEN": "Length",
	"TMED": "Media type",
	"TOAL": "Original album title",
	"TOFN": "Original filename",
	"TOLY": "Original lyricist",
	"TOPE": "Original artist",
	"TORY": "Original release year",
	"TOWN": "File owner",
	"TPE1": "Artist",
	"TPE2": "Band/orchestra/accompaniment",
	"TPE3": "Conductor/performer refinement",
	"TPE4": "Interpreted, remixed, or otherwise modified by",
	"TPOS": "Part of a set",
	"TPUB": "Publisher",
	"TRCK": "Track",
	"TRDA": "Recording dates",
	"TRSN": "Internet radio station name",
	"TRSO": "Internet radio station owner",
	"TSIZ": "Size",
	"TSRC": "ISRC (international standard recording code)",
	"TSSE": "Software/Hardware and settings used for encoding",
	"TYER": "Year",
	"TXXX": "User defined text information frame",
	"UFID": "Unique file identifier",
	"USER": "Terms of use",
	"USLT": "Unsychronized lyric/text transcription",
	"WCOM": "Commercial information",
	"WCOP": "Copyright/Legal information",
	"WOAF": "Official audio file webpage",
	"WOAR": "Official artist/performer webpage",
	"WOAS": "Official audio source webpage",
	"WORS": "Official internet radio station homepage",
	"WPAY": "Payment",
	"WPUB": "Publishers official webpage",
	"WXXX": "User defined URL link frame",
}
